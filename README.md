# arp-spoof

A small arp spoof tool written in Rust using raw sockets with libc.

## Usage

**Must be run as root.**

```bash
# enable packet forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward

arp-spoof <interface> <target-ip> <gateway-ip>
```
